const { model } = require("../models/sms");
const fetch = require('node-fetch');
const axios = require('axios');
var FormData = require('form-data');

/*Cargar formulario de login*

 * Se encarga de renderizar el diseño de login
 * @param {*} req 
 * @param {*} res 
 */
async function login(req, res) {
    res.render("login");
};

/*Valida los datos y cargar datos*

 * Verifica si los datos solo los correctos para entrar
  la parte de administración de los contario mostara un mensaje
 * @param {*} req 
 * @param {*} res 
 */
async function loginUser(req, res) {

    const { user, pass } = req.body;
    if (user === process.env.USER && pass === process.env.PASSWORD) {
        getChannels(req, res);
    } else {
        var error = "Usuario o Constaseña incorrecto";
        res.render("login", { error });
    }

};

/*Obtine los channels y carga los datos *

 * Metodo se encarga de obtener los channels y luego envia los datos al parte grafica
 * @param {*} req 
 * @param {*} res 
 */
async function getChannels(req, res) {
    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels';

    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        'Content-Type': 'application/json; charset=utf-8'
    }

    var config = {
        method: 'get',
        url: url,
        headers: headers,
    };
    await axios(config).then(function (response) {
        let channels = response.data;
        // console.log(JSON.stringify(channels));  
        res.render("adminLogin", { channels });
    })
        .catch(function (error) {
            console.log(error);
        });
}

/*Carga los channels sin ser usuario*
 * Se encarga de traer los channels sin ser usuarios y envia los datos a la parte grafica
 * @param {*} req 
 * @param {*} res 
 */
async function getChannels_nouser(req, res) {
    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels';

    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        'Content-Type': 'application/json; charset=utf-8'
    }

    var config = {
        method: 'get',
        url: url,
        headers: headers,
    };
    await axios(config).then(function (response) {
        let channels = response.data;
        // console.log(JSON.stringify(channels));  
        res.render("channels", { channels });
    })
        .catch(function (error) {
            console.log(error);
        });
}

/*Llama a crear channel y cargar channels*

 * Crear los channels y obtiene los channels 
 * @param {*} req 
 * @param {*} res 
 */
const create_channel = (req, res) => {
    create_data(req, res);
    getChannels(req, res);
};

/*Crear canales*

 * Metodo se encarga de crear el canal 
 * @param {*} req 
 * @param {*} res 
 */
async function create_data(req, res) {
    const { channel } = req.body;
    var data = new FormData();

    data.append('FriendlyName', channel);

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels';
    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        ...data.getHeaders()

    };

    var config = {
        method: 'post',
        url: url,
        headers: headers,
        data: data
    };

    await axios(config)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log(error);
        });
};

/*Carga formulario de Editar Channel*

 * Metodo en carga los channels por medio de los datos de la url
 * @param {*} req 
 * @param {*} res 
 */
const form_edit_channel = (req, res) => {

    var sid = req.params.sid;
    var friendly_name = req.params.friendly_name;
    res.render('editchannel', { sid, friendly_name });

};

/*Llama editar el channel y cargar el channel*

 * Metodo se encarga de llamar a Editar los channel y luego cargar channel
 * @param {*} req 
 * @param {*} res 
 */
const edit_channel = (req, res) => {

    edit_data(req, res);
    getChannels(req, res);

}

/*Editar el Channel*

 * Metodo se encarga de editar el channel
 * @param {*} req 
 * @param {*} res 
 */
async function edit_data(req, res) {

    const { sid, channel } = req.body;
    var data = new FormData();

    data.append('FriendlyName', channel);

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + sid + '';

    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        ...data.getHeaders()
    };
    var config = {
        method: 'post',
        url: url,
        headers: headers,
        data: data
    };

    await axios(config)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log(error);
        });
}

/*Llama a eliminar channel y luego cargar los channels*
 * Metodo se llama a eliminar los channels y luego cargar los channles
 * @param {*} req 
 * @param {*} res 
 */
const delete_channel = function (req, res) {

    delete_data(req, res);
    getChannels(req, res);

}

/*Eliminar los channels*

 * Metodo se encarga de eliminar los channels
 * @param {*} req 
 * @param {*} res 
 */
async function delete_data(req, res) {
    var idchannel = req.params.sid;

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + idchannel;
    var data = new FormData();

    data.append('sid', idchannel);
    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        ...data.getHeaders()

    };


    var config = {
        method: 'delete',
        url: url,
        headers: headers,
        data: data
    };

    await axios(config)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log(error);
        });
}

module.exports = {
    login,
    loginUser,
    getChannels,
    getChannels_nouser,
    create_channel,
    form_edit_channel,
    edit_channel,
    delete_channel
}