const { model } = require("../models/sms");
const fetch = require('node-fetch');
const axios = require('axios');
var FormData = require('form-data');
const { param } = require("../server");

/*Cargar Formularios de Crear Usuario*

 * Metodo se encargar de cargar el formulario de crear usuario, asi mismo 
 le pasa los paramentos del url
 * @param {*} req 
 * @param {*} res 
 */
const form_create_User = function (req, res) {
    var idchannel = req.params.sid;
    var channel = req.params.friendly_name;
    res.render("createUser", { idchannel, channel });
};

/* Llama a Crear Usuario y Carga de Mensajes*

 * Metodo que crea el usuario y carga los datos de mensajes y usuario
 * @param {*} req 
 * @param {*} res 
 */
const create_User = function (req, res) {

    create_Data(req, res);
    get_Messages(req, res);

};

/*Creacion de Usuario*

 * Metodo que crea el usuario
 * @param {*} req 
 * @param {*} res 
 */
async function create_Data(req, res) {
    
    const { idchannel, channel, identity } = req.body;
    var data = new FormData();

    data.append('Identity', identity);

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + idchannel + "/Members";
    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        ...data.getHeaders()

    };

    var config = {
        method: 'post',
        url: url,
        headers: headers,
        data: data
    };

    await axios(config)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            if (error.response.status == 409) {
                console.log("Usuario Existe");
            }
            else {
                console.log(error);
            }
        });
}

/*Cargar mensajes y luego llama crear usuario *

 * Metodo que obtiene los mensajes y llama los usuarios
 * @param {*} req 
 * @param {*} res 
 */
const get_Messages = function (req, res) {
    var identity;
    var channel;
    var idchannel;
    if (req.body.idchannel) {
        idchannel = req.body.idchannel;
        channel = req.body.channel;
        identity = req.body.identity;

    } else {
        idchannel = req.params.idchannel;
        channel = req.params.channel;
        identity = req.params.identity;
    }

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + idchannel + '/Messages';

    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        'Content-Type': 'application/json; charset=utf-8'
    }

    var config = {
        method: 'get',
        url: url,
        headers: headers,
    };
    axios(config).then(function (response) {

        let sms = response.data;
        get_Users(req, res, sms);

    })
        .catch(function (error) {
            console.log(error);
        });

};

/*Obtiene los miembros y carga datos*

 * Metodo que traer los miembros del channel y posteriormente
 *  realiza un render para cargar todos los mensajes y los miembros en el chat
 * 
 * @param {} req 
 * @param {} res 
 * @param {String} sms Lleva los mensajes del chat 
 */
const get_Users = async (req, res, sms) => {
    var identity;
    var channel;
    var idchannel;
    if (req.body.idchannel) {
        idchannel = req.body.idchannel;
        channel = req.body.channel;
        identity = req.body.identity;

    } else {
        idchannel = req.params.idchannel;
        channel = req.params.channel;
        identity = req.params.identity;
    }

    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + idchannel + '/Members';
    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        'Content-Type': 'application/json; charset=utf-8'
    }

    var config = {
        method: 'get',
        url: url,
        headers: headers,
    };
    await axios(config).then(function (response) {
        let users = response.data;
        res.render("channelMessages", { idchannel, channel, identity, sms, users });
    })
        .catch(function (error) {
            console.log(error);
        });
};

/*Llama a crear mensajes y llama a traer mensajes*

 * Metodo que llama a crea los mensajes del chat y luego llama a traer mensajes 
 * @param {*} req 
 * @param {*} res 
 */
async function create_Message(req, res) {

    const { idchannel, channel, identity } = req.body;
    create_data_Message(req, res);
    get_Messages(req, res);
}

/*Crea los mensajes *
 * Metodo se encarga de crear los mensajes del channel
 * @param {*} req 
 * @param {*} res 
 */
async function create_data_Message(req, res) {

    const { idchannel, message, identity } = req.body;
    var url = 'https://chat.twilio.com/v2/Services/' + process.env.TWILIO_SERVICE_SID + '/Channels/' + idchannel + '/Messages';

    var data = new FormData();

    data.append('Body', message);
    data.append('From', identity);
    var headers = {
        'Authorization': 'Basic ' + Buffer.from(process.env.TWILIO_ACCOUNT_SID + ":" + process.env.TWILIO_AUTH_TOKEN).toString('base64'),
        ...data.getHeaders()
    }
    var config = {
        method: 'post',
        url: url,
        headers: headers,
        data: data
    };
    await axios(config).then(function (response) {
        return response.data;
    })
        .catch(function (error) {
            return Promise.reject(error.response);
            console.log(error);
        });
}

/**
 * Exportar los metodos
 */
module.exports = {
    form_create_User,
    create_User,
    create_Message,
    get_Messages
}