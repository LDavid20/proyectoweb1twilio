const { Router } = require("express");
const router = Router();

const {
  indexController,
  postMessage,
  receiveMessage
} = require("../controllers/index.controller");
// const {
//   login,
//   loginUser,
// } = require("../controllers/startController");
// const { render } = require("../server");



// Main Routes
// router.get("/",indexController);
router.get("/",(req, res) => {
  res.render('login')
});

// Send an SMS
router.post("/send-sms", send_login);

// Receive an SMS
router.post('/sms', receiveMessage);

module.exports = router;
