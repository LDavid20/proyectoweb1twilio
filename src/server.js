const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const morgan = require("morgan");
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");
const {
  base64decode
} = require('nodejs-base64');
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();

//CLASE CONTROLLES

// Channels

const {
  login,
  loginUser,
  getChannels,
  getChannels_nouser,
  create_channel,
  form_edit_channel,
  edit_channel,
  delete_channel
} = require("./controllers/channelController.js");

// Messages
const {
  form_create_User,
  create_User,
  create_Message,
  get_Messages
} = require("./controllers/messagesController.js");

// Settings
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.engine(
  ".hbs",
  exphbs({
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    defaultLayout: "main",
    extname: ".hbs",
    helpers: require("./libs/handlebars"),
  })
);
app.set("view engine", ".hbs");

// Middlewares
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// listen to the task request

// ADMIN 
app.get("/", login);
app.post("/send_login", loginUser);
app.get("/adminLogin", getChannels);
app.post("/create_channel", create_channel);
app.post("/edit_channel", edit_channel);
app.get("/edit_channel/:sid/:friendly_name", form_edit_channel);
app.get("/delete_channel/:sid", delete_channel);


// USER
app.get("/channels", getChannels_nouser);
app.get("/update_channels", getChannels_nouser);
app.get("/create_user/:sid/:friendly_name", form_create_User);
app.post("/messages", create_User);
app.get("/update_Messages/:idchannel/:channel/:identity", get_Messages);
app.post("/create_message", create_Message);

// static files
app.use(express.static(path.join(__dirname, "public")));

module.exports = app;
